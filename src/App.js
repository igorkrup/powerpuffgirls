import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import './scss/index.scss';

import Show from './components/Show';
import Episode from './components/Episode';

const App = ({ store }) => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="container">
          <Route path="/" exact={true} component={Show} />
          <Route
            path="/episodes/:id"
            exact={true}
            render={props => <Episode id={props.match.params.id} />}
          />
        </div>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
