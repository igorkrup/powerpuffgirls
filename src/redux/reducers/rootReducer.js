import { combineReducers } from 'redux';

import episodes from './episodes';
import show from './show';

const rootReducer = combineReducers({
  episodes,
  show
});

export default rootReducer;
