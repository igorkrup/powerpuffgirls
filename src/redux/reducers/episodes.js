// Reducer for episode

const reducerName = 'episodes';

export const FETCH = `${reducerName}/FETCH`;
export const FETCH_REQUEST = `${reducerName}/FETCH_REQUEST`;
export const FETCH_SUCCESS = `${reducerName}/FETCH_SUCCESS`;
export const FETCH_ERROR = `${reducerName}/FETCH_ERROR`;

const initialState = {
  loading: false,
  loaded: false,
  episodesById: {}
};

/**
 * Reducer function
 */
export default function reducer(state = initialState, action) {
  const { payload, type } = action;

  switch (type) {
    case FETCH_REQUEST:
      return {
        ...state,
        loading: true
      };

    case FETCH_SUCCESS:
      let episodesById = {};
      payload.forEach(episode =>
        Object.assign(episodesById, { [episode.id]: episode })
      );

      return {
        ...state,
        episodesById
      };

    case FETCH_ERROR: {
      return {
        ...state,
        error: true,
        loading: false
      };
    }

    default:
      return state;
  }
}

// Action creators
export const fetchEpisodes = showId => ({
  type: FETCH,
  meta: { endpoint: `/shows/${showId}/episodes` }
});

// State selectors
export const getEpisodes = state => state.episodes.episodesById;
export const isLoaded = state => state.episodes.loaded;
export const isLoading = state => state.episodes.loading;
export const getEpisode = (state, episodeId) =>
  state.episodes.episodesById[episodeId];
