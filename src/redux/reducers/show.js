// Reducer for episode

const reducerName = 'show';

export const FETCH = `${reducerName}/FETCH`;
export const FETCH_REQUEST = `${reducerName}/FETCH_REQUEST`;
export const FETCH_SUCCESS = `${reducerName}/FETCH_SUCCESS`;
export const FETCH_ERROR = `${reducerName}/FETCH_ERROR`;

const initialState = {
  loading: false,
  loaded: false,
  data: {}
};

/**
 * Reducer function
 */
export default function reducer(state = initialState, action) {
  const { payload, type } = action;

  switch (type) {
    case FETCH_REQUEST:
      return {
        ...state,
        loading: true
      };

    case FETCH_SUCCESS:
      return {
        ...state,
        data: payload,
        loading: false,
        loaded: true
      };

    case FETCH_ERROR: {
      return {
        ...state,
        error: true,
        loading: false
      };
    }

    default:
      return state;
  }
}

// Action creators
export const fetchShow = showId => ({
  type: FETCH,
  meta: { endpoint: `/shows/${showId}` }
});

// State selectors
export const isLoaded = state => state.show.loaded;
export const isLoading = state => state.show.loading;
export const getShow = state => state.show.data;
