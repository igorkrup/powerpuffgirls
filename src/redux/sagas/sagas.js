import { call, select, put, all, takeEvery } from 'redux-saga/effects';
import { apiCall } from '../../services/api';

/**
 * Returns action type checker function for given pattern
 * @param pattern
 * @returns {function(*): boolean}
 */
const matchesPattern = pattern => action =>
  action.type.split('/')[1] === pattern;

/**
 * Gets scope name from action type
 * @param type
 * @returns {*|string}
 */
const getActionScope = type => type.split('/')[0];

/**
 * Returns "loading" state selector function for scope
 * @param scope§
 * @returns {function(*): boolean}
 */
const isScopeLoading = scope => state => state[scope].loading;

/**
 * Generic watches for FETCH actions for all scopes
 * @param action - action passed by takeEvery
 */
function* watchFetchAction(action) {
  const {
    type,
    meta: { endpoint }
  } = action;
  const scope = getActionScope(type);
  const loading = yield select(isScopeLoading(scope));
  if (!loading) {
    yield put({ type: `${scope}/FETCH_REQUEST` });

    const response = yield call(apiCall, { endpoint });

    if (response.response.ok && response.json) {
      yield put({ type: `${scope}/FETCH_SUCCESS`, payload: response.json });
    }
  }
}

export default function*() {
  yield all([
    /**
     * Watch every action having FETCH type and fork new saga for each one.
     * takeEvery adds matched action as last argument to forked saga
     */
    takeEvery(matchesPattern('FETCH'), watchFetchAction)
  ]);
}
