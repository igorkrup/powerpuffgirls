import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './redux/reducers/rootReducer';
import sagas from './redux/sagas/sagas';

const configureStore = (initialState, middleware) =>
  createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware))
  );

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({}, [sagaMiddleware]);
sagaMiddleware.run(sagas);

ReactDOM.render(<App store={store} />, document.getElementById('root'));
registerServiceWorker();
