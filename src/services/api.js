const apiRoot = 'http://api.tvmaze.com';

export const apiCall = ({ endpoint, method = 'GET' }) =>
  fetch(apiRoot + endpoint, {
    method
  })
    .then(response => response.json().then(json => ({ json, response })))
    .catch(error => error);
