import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const EpisodeItem = ({ name, id, season, number }) => (
  <Link className="episode-item" to={{ pathname: `/episodes/${id}` }}>
    <span className="episode-item__season" data-label="Season:">
      {season}
    </span>
    <span className="episode-item__number" data-label="Episode:">
      {number}
    </span>
    <span className="episode-item__name">{name}</span>
  </Link>
);

EpisodeItem.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  season: PropTypes.number.isRequired,
  number: PropTypes.number.isRequired
};

export default EpisodeItem;
