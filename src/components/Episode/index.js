import React, { Component } from 'react';
import { connect } from 'react-redux';
import { size } from 'lodash';

import EpisodeDetails from './EpisodeDetails';

import * as episodeModule from '../../redux/reducers/episodes';
import { SHOW_ID } from '../Show';

class Episode extends Component {
  componentDidMount() {
    const { episodes, loading, fetchEpisodes } = this.props;
    if (!size(episodes) && !loading) {
      fetchEpisodes(SHOW_ID);
    }
  }

  render() {
    const { episodeDetails } = this.props;
    if (!size(episodeDetails)) {
      return <div className="loading">Loading...</div>;
    }
    return <EpisodeDetails {...episodeDetails} />;
  }
}

export default connect(
  (state, props) => ({
    episodes: episodeModule.getEpisodes(state),
    episodeDetails: episodeModule.getEpisode(state, props.id),
    loading: episodeModule.isLoading(state)
  }),
  {
    fetchEpisodes: episodeModule.fetchEpisodes
  }
)(Episode);
