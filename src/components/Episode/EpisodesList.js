import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import EpisodeItem from './EpisodeItem';

const EpisodesList = ({ episodes }) => (
  <Fragment>
    <h3>Episodes:</h3>
    <section className="episodes">
      {Object.entries(episodes).map(([id, episode]) => (
        <EpisodeItem key={`episode-${id}`} {...episode} />
      ))}
    </section>
  </Fragment>
);

EpisodesList.propTypes = {
  episodes: PropTypes.object.isRequired
};

export default EpisodesList;
