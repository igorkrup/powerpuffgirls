import React from 'react';
import PropTypes from 'prop-types';

export const EpisodeDetails = ({ name, summary, image }) => (
  <section className="article">
    <article>
      <h1 className="article__title">{name}</h1>
      {image && (
        <div className="article__image">
          <img src={image.medium || image.original} alt={name} />
        </div>
      )}
      <div
        className="article__description"
        dangerouslySetInnerHTML={{ __html: summary }}
      />
    </article>
  </section>
);

EpisodeDetails.propTypes = {
  name: PropTypes.string.isRequired,
  summary: PropTypes.string,
  image: PropTypes.object
};

export default EpisodeDetails;
