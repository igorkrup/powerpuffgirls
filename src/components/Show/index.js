import React, { Component } from 'react';
import { connect } from 'react-redux';
import { size } from 'lodash';

import ShowDetails from './ShowDetails';
import EpisodesList from '../Episode/EpisodesList';

import * as showModule from '../../redux/reducers/show';
import * as episodeModule from '../../redux/reducers/episodes';

export const SHOW_ID = 6771;

class Show extends Component {
  componentDidMount() {
    const { fetchShow, showLoaded, fetchEpisodes, episodesLoaded } = this.props;
    if (!showLoaded) {
      fetchShow(SHOW_ID);
    }
    if (!episodesLoaded) {
      fetchEpisodes(SHOW_ID);
    }
  }

  render() {
    const { showDetails, episodes } = this.props;

    if (size(showDetails) && size(episodes)) {
      return (
        <div className="container">
          <ShowDetails {...showDetails} />
          <EpisodesList episodes={episodes} />
        </div>
      );
    } else {
      return <div className="loading">Loading...</div>;
    }
  }
}

export default connect(
  state => ({
    showLoaded: showModule.isLoaded(state),
    showLoading: showModule.isLoading(state),
    showDetails: showModule.getShow(state),
    episodesLoading: episodeModule.isLoading(state),
    episodesLoaded: episodeModule.isLoaded(state),
    episodes: episodeModule.getEpisodes(state)
  }),
  {
    fetchShow: showModule.fetchShow,
    fetchEpisodes: episodeModule.fetchEpisodes
  }
)(Show);
