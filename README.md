Author: Igor Krupa

#### Installation

1. Clone this repo.
2. Open terminal and navigate to directory.
3. Run `npm install`.
4. Run `npm start`.

#### Description

A lot of improvements in UI could be done - I limited it to expected minimum.